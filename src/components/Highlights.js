import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4} className="p-3">
				<Card className="cardHighlight p-3">
				  
				  <Card.Body>
				    <Card.Title><h2>Learn from Home</h2></Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				      tempor incididunt ut labore et dolore magna aliqua.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4} className="p-3">
				<Card className="cardHighlight p-3">
				  
				  <Card.Body>
				    <Card.Title><h2>Study now Pay Later</h2></Card.Title>
				    <Card.Text>
				      Duis aute irure dolor in reprehenderit in voluptate velit esse
				      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4} className="p-3">
				<Card className="cardHighlight p-3">
				  
				  <Card.Body>
				    <Card.Title><h2>Be part of our community</h2></Card.Title>
				    <Card.Text>
				      Ut enim ad minim veniam,
				      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				      consequat.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}