import { useState, Fragment } from 'react';
// Proptypes - used to validate props
import PropTypes from 'prop-types';
import { Button, Row, Col, Card, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import imageSpace from './Space.jpg';



export default function ProductCard({productProp}){



	const [count, setCount] = useState(0);

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	function enroll(){

		if(count < 30){
			setCount(count + 1);
			localStorage.setItem(`countOf${_id}`, count);
		}

		else{
			alert("There are no more seats available");
		}

		console.log(count);
	}
	

	const {_id, name, description, price } = productProp;

	return (
		
			<Fragment>
				<Col xs={12} md={4} className="p-5">
					<Card className="cardHighlight border-0">
					
					<Card.Img className="image" onClick={handleShow} variant="top" src={imageSpace} />
			
					
					  <Card.Body className="">
					    <Card.Title className="text-center pb-4 title-card" onClick={handleShow}><h5>{name}</h5></Card.Title>
					    <Card.Text>
					      <div className="d-flex">      	
					      	
					      </div>

					     </Card.Text>
					      
					  </Card.Body>
					</Card>
				</Col>

				<Modal show={show} onHide={handleClose}>
				        <Modal.Header closeButton>
				          <Modal.Title>{name}</Modal.Title>
				        </Modal.Header>
				        <Modal.Body>
				        	<ul>
				        		<li><strong>Description:</strong></li>
				        		<li>{description}</li>
				        	</ul>

				        	<ul>
				        		<li><strong>Price:</strong></li>
				        		<li>PhP {price}</li>
				        	</ul>
				        </Modal.Body>
				        <Modal.Footer>
				          <Button variant="primary" onClick={enroll}>Add</Button>
				          <Button variant="secondary" onClick={handleClose}>
				            Close
				          </Button>
				        </Modal.Footer>
				      </Modal>
			</Fragment>
			
	
	)
}




// Checks the validity of the PropTypes
ProductCard.propTypes = {
	// "shape" method is used to check if a prop object conforms to a specific shape
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}