import { useState, useEffect, useContext } from 'react';
import { Form, Button, ListGroup, Dropdown } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

// placholders only

export default function Admin(){


	return (
		
			<Form className="mt-3 pt-5 mt-5">
		      
			  <Form.Group>
			  	
			  </Form.Group>

			  <Form.Group className="mt-5 pt-5 d-md-flex justify-content-around">


			  	<Dropdown className="d-md-none mb-5 p-5 text-center">
			  	  <Dropdown.Toggle variant="Danger" id="dropdown-basic">
			  	    <h1>Products</h1>
			  	  </Dropdown.Toggle>

			  	  <Dropdown.Menu className="text-center">
			  	    <Dropdown.Item href="#/action-1">View all products</Dropdown.Item>
			  	    <Dropdown.Item href="#/action-2">View one product</Dropdown.Item>
			  	    <Dropdown.Item href="#/action-3">Create a Product</Dropdown.Item>
			  	    <Dropdown.Item href="#/action-3">Update a product</Dropdown.Item>
			  	  </Dropdown.Menu>
			  	</Dropdown>


			  	<Dropdown className="d-md-none mb-5 p-5 text-center">
			  	  <Dropdown.Toggle variant="Danger" id="dropdown-basic">
			  	    <h1>Users</h1>
			  	  </Dropdown.Toggle>

			  	  <Dropdown.Menu className="text-center">
			  	    <Dropdown.Item href="#/action-1">View all users</Dropdown.Item>
			  	    <Dropdown.Item href="#/action-2">View one user</Dropdown.Item>
			  	    <Dropdown.Item href="#/action-3">Set user to admin</Dropdown.Item>
			  	  </Dropdown.Menu>
			  	</Dropdown>

			  	<Dropdown className="d-md-none mb-5 p-5 text-center">
			  	  <Dropdown.Toggle variant="Danger" id="dropdown-basic">
			  	    <h1>Orders</h1>
			  	  </Dropdown.Toggle>

			  	  <Dropdown.Menu className="text-center">
			  	    <Dropdown.Item href="#/action-1">View all orders</Dropdown.Item>
			  	  </Dropdown.Menu>
			  	</Dropdown>

			  	<ListGroup defaultActiveKey="#link1" className="d-none d-md-block">
			  		<Form.Text className="text-center mb-4">
			  			<h1>Products</h1>
			  		</Form.Text>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link1">
			  	      View all products
			  	    </ListGroup.Item>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link2">
			  	      View one product
			  	    </ListGroup.Item>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link3">
			  	      Create a Product
			  	    </ListGroup.Item>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link4">
			  	      Update a product
			  	    </ListGroup.Item>
			  	</ListGroup>

			  	
			  	<ListGroup defaultActiveKey="#link2" className="d-none d-md-block">
			  		<Form.Text className="text-center mb-4">
			  			<h1>Users</h1>
			  		</Form.Text>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link5">
			  	      View all users
			  	    </ListGroup.Item>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link6">
			  	      View one user
			  	    </ListGroup.Item>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link7">
			  	      Set user to admin
			  	    </ListGroup.Item>
			  	</ListGroup>

			  	<ListGroup defaultActiveKey="#link3" className="d-none d-md-block">
			  		<Form.Text className="text-center mb-4">
			  			<h1>Orders</h1>
			  		</Form.Text>
			  	    <ListGroup.Item className="rounded mb-3" action href="#link8">
			  	      View all orders
			  	    </ListGroup.Item>
			  	    
			  	</ListGroup>
			  </Form.Group>

			</Form>


	)
}