import { Fragment, useEffect, useState } from 'react';
import Highlights from '../components/Highlights';
import Filter from '../components/Filter'
import ProductCard from '../components/ProductCard';
import { Button, Row, Col, Card, Container } from 'react-bootstrap';
import Checkout from '../components/Checkout';



// export default function Product(){

// 	return (
// 		<Fragment>
// 			<Filter />
// 			<Highlights />	
// 		</Fragment>
// 	);
// }


export default function Product(){
	
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/courses/all')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			
			setProducts(
				data.map(product => {
					return (
						<ProductCard key={product.id} productProp = {product} />
					)
				})
			);
		})
	}, [])

	



	return (
		<Fragment>
			<div className="text-md-left text-center d-flex">
				<h1 className="mt-5">Products</h1>
				<div className="ml-auto align-self-end">
					<div className="pr-5 mr-5 pb-3">
						<Checkout />
					</div>
				</div>
			</div>
	
			<Filter className="" />
			
			
			<Row className="mt-3 mb-3">
				{products}
			</Row>
			
		</Fragment>
	)
}